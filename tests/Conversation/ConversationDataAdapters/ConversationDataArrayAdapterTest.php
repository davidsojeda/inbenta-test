<?php


use Conversation\ConversationDataAdapters\ConversationDataArrayAdapter;

class ConversationDataArrayAdapterTest extends \PHPUnit_Framework_TestCase
{
    public function testGetEmptyConversation()
    {
        $convAdapter = new ConversationDataArrayAdapter();
        $this->assertCount(0, $convAdapter->getConversation());
    }

    public function testAddConversationMessage()
    {
        $convAdapter = new ConversationDataArrayAdapter();
        $convAdapter->addUserConversationMessage('Just testing');
        $conv = $convAdapter->getConversation();
        $this->assertCount(1, $conv);
        $this->assertEquals($conv[0], ['user' => 'user', 'message' => 'Just testing']);
    }


    public function testSetAttributeValue()
    {
        $convAdapter = new ConversationDataArrayAdapter();
        $convAdapter->setAttributeValue('lorem', 'ipsum');
        $convAdapter->setAttributeValue('foo', 'bar');
        $attributes = $convAdapter->getAttributes();
        $this->assertCount(2, $attributes);
        $this->assertArraySubset(
            ['lorem' => [
                'value'     => 'ipsum',
                'retrieved' => true
            ], 'foo' => [
                'value'     => 'bar',
                'retrieved' => true
            ] ],
            $attributes
        );
    }

    public function testSetAttributeAsAsked()
    {
        $convAdapter = new ConversationDataArrayAdapter();
        $convAdapter->setAttributeAsAsked('foo');
        $attributes = $convAdapter->getAttributes();
        $this->assertCount(1, $attributes);
        $this->assertArraySubset( ['foo' => ['asked' => true, 'retrieved' => false ]], $attributes);
    }
}