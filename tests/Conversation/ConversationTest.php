<?php

use Conversation\Conversation;
use Conversation\ConversationDataAdapters\ConversationDataArrayAdapter;
use Conversation\AttributeQuestion\AgeAttributeQuestion;

class ConversationTest extends \PHPUnit_Framework_TestCase
{
    public function testConversationIsStarted()
    {
        $conv = $this->getConversation();
        $this->assertFalse($conv->isStarted());
        $conv->talk('Hi');
        $this->assertTrue($conv->isStarted());
    }

    public function testWelcomeMessage()
    {
        $conv = $this->getConversation();
        $conv->setWelcomeMessage('Hola!');
        $response = $conv->talk('Hi');
        $this->assertArraySubset(['Hola!'], $response);
    }

    public function testConversation()
    {
        $conv = $this->getConversation();
        $conv->setEndMessage('{age}');
        $question = new AgeAttributeQuestion();
        $conv->addAttributeQuestion('age', $question);
        $response = $conv->talk('Hi');
        $this->assertArraySubset(['Hi', $question->getQuestion()], $response);
        $response = $conv->talk('1000');
        $this->assertArraySubset([$question->getErrorMessage()], $response);
        $response = $conv->talk('10');
        $this->assertArraySubset([10], $response);
    }

    /**
     * @expectedException Exception
     */
    public function testValidateUserMessage()
    {
        $conv = $this->getConversation();
        $conv->validateUserMessage('');
    }

    private function getConversation()
    {
        return new Conversation(new ConversationDataArrayAdapter());
    }



}