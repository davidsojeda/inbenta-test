<?php

use Conversation\Attribute;

class AttributeTest extends \PHPUnit_Framework_TestCase
{
    public function testEmptyConstructor()
    {
        $attribute = new Attribute('foo');
        $this->assertFalse($attribute->isAsked());
        $this->assertFalse($attribute->isRetrieved());
        $this->assertEquals('foo', $attribute->getName());
    }

    public function testConstructor()
    {
        $data = [
            'asked'     => true,
            'retrieved' => true,
            'value'     => 'bar'
        ];
        $attribute = new Attribute('foo', $data);
        $this->assertTrue($attribute->isAsked());
        $this->assertTrue($attribute->isAsked());
        $this->assertEquals('bar', $attribute->getValue());
    }


}