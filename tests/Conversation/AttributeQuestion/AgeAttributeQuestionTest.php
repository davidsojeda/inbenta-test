<?php

use Conversation\AttributeQuestion\AgeAttributeQuestion;

class AgeAttributeQuestionTest extends \PHPUnit_Framework_TestCase
{
    public function testAgeValid()
    {
        $ageAttribute = new AgeAttributeQuestion();
        $this->assertFalse($ageAttribute->validate(0));
        $this->assertTrue($ageAttribute->validate(15));
        $this->assertFalse($ageAttribute->validate(150));
    }
}