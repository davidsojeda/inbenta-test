<?php

use Conversation\AttributeQuestion\EmailAttributeQuestion;

class EmailAttributeQuestionTest extends \PHPUnit_Framework_TestCase
{
    public function testEmailValid()
    {
        $emailAttribute = new EmailAttributeQuestion();
        $this->assertFalse($emailAttribute->validate('afhajskdhfjkasdf'));
        $this->assertFalse($emailAttribute->validate('afhajskdhfjkasdf@asdf'));
        $this->assertTrue($emailAttribute->validate('john@asdfasdf.com'));
    }
}