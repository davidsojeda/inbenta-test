$(document).ready(function() {

    var apiUrl = 'index.php/';
    var botName = $('.bot').data('bot-name');

    function addUserMessage(message) {
        var userMessage = `
        <div class="message-item message-item--sent">
         <div class="message">
          <span class="message__text">` + message + `</span>
         </div>
        </div>`;
        addChatMessage(userMessage);
    }

    function addBotMessage(message) {
        var botMessage = `
        <div class="message-item message-item--received">
         <div class="message">
          <span class="message__label">` + botName + `</span>
          <span class="message__text">` + message + `</span>
         </div>
        </div>`;
        addChatMessage(botMessage);
    }

    function addChatMessage(message) {
        $('.bot__messages_list').append(message);
        chatScroll();
    }

    function chatScroll() {
        var elem = $('.bot__messages');
        elem.animate({
            scrollTop: elem.prop('scrollHeight')
        });
    }

    chatScroll();

    $('form').submit(function(e) {
        e.preventDefault();
        var messageInput = $('.bot__form__input');
        var message = messageInput.val();
        addUserMessage(message);
        messageInput.val('');
        messageInput.focus();
        var posting = $.post( apiUrl + 'talk', { message: message });
        posting.done(function(response) {
            for( var i in response.messages ) {
                addBotMessage(response.messages[i]);
            }
        });
    });

});