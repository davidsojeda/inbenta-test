# Inbenta Chatbot Exercise

## Running the code
Exercise was created and tested in a Vagrant box with PHP 5.5.9 (https://github.com/mattandersen/vagrant-lamp/releases/tag/PHP55-2016-05-11).

It should be compatible as well with the configuration proposed in the exercise.

- Execute `composer install` to install dependencies.
- You can execute project tests by running `phpunit`.

## Proposed solution

This solutions allows:
- Create new attribute questions with custom validation rules, question and error messages (just by implementing `AttributeQuestionInterface`).
- Customize welcome and end messages. End message can display attributes retrieved just following the syntax `Goodbye, {name}`.
- Customize conversation workflow by modifying `talk` method in `Conversation` class.
- Replace `ConversationData` container (in the main controller we are using a session-based implementation, in test we are using an array implementation, but we can create new implementations just following `ConversationDataInterface`)

## Minor things to be improved
- Performance
- End message error handling (in case we try to display an attribute not retrieved)
- We can use Silex DI to resolve ConversationData dependency on Conversation (currently we provide manually the instance)
- Frontend uses very simple jQuery scripting, and duplicates message view template used also in backend. We can use define a method in controller to get asynchronously all the conversation messages (in fact, we can use ConversationData->getConversation method).