<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

use Conversation\Conversation;
use Conversation\ConversationDataAdapters\ConversationDataSessionAdapter;
use Conversation\AttributeQuestion\NameAttributeQuestion;
use Conversation\AttributeQuestion\EmailAttributeQuestion;
use Conversation\AttributeQuestion\AgeAttributeQuestion;

// Main route
$app->match('/', function (Application $app, Request $request) {
    return $app['twig']->render('index.html', array(
        'title' => 'Bot exercise',
        'name' => 'Paloma',
        'messages' => (new ConversationDataSessionAdapter($app['session']))->getConversation()
    ));
});

$app->match('/talk', function (Application $app, Request $request) {

    $conversation = new Conversation(new ConversationDataSessionAdapter($app['session']));
    $conversation->addAttributeQuestion('name', new NameAttributeQuestion());
    $conversation->addAttributeQuestion('email', new EmailAttributeQuestion());
    $conversation->addAttributeQuestion('age', new AgeAttributeQuestion());
    $conversation->setWelcomeMessage('Hello! I will ask you some questions ok?');
    $conversation->setEndMessage(
        'Thanks! Now I know you better. Your name is {name}, you are {age} old and I can contact you on {email}!'
    );

    $botResponses = $conversation->talk($request->get('message'));

    $data = array(
        'success' => true,
        'messages' => $botResponses
    );

    return $app->json($data);
});
