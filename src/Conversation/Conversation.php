<?php

namespace Conversation;

use Conversation\AttributeQuestionInterface;
use Conversation\ConversationDataInterface;

/**
 * Handles a conversation between a user and a bot.
 * We can add custom questions (implementing AttributeQuestionInterface), and customized welcome and end messages
 *
 * Class Conversation
 * @package Conversation
 */
class Conversation
{
    /** @var Attribute[] */
    private $attributes = [];

    /** @var \Conversation\ConversationDataInterface  */
    private $conversationData;

    private $welcomeMessage = 'Hi';
    private $endMessage = 'Bye';

    /**
     * Conversation constructor.
     * @param \Conversation\ConversationDataInterface $conversationData
     */
    public function __construct(ConversationDataInterface $conversationData)
    {
        $this->conversationData = $conversationData;

        // If there are initialized attributes found in conversation data, fetch them
        foreach($conversationData->getAttributes() as $attributeName => $attribute) {
            $this->attributes[$attributeName] = new Attribute($attributeName, $attribute);
        }
    }

    /**
     * Add a new attribute to be asked in the conversation
     *
     * @param $name Name of the attribute
     * @param \Conversation\AttributeQuestionInterface $attribute
     */
    public function addAttributeQuestion($name, AttributeQuestionInterface $attribute)
    {
        if(! array_key_exists($name, $this->attributes)) {
            $this->attributes[$name] = new Attribute($name);
        }
        $this->attributes[$name]->setAttributeQuestion($attribute);
    }

    /**
     * Handles all conversation workflow. Return an array with bot responses
     *
     * @param $message User message
     * @return array Bot Responses
     */
    public function talk($message)
    {
        // At first, check some basic validation rules. We don't want to handle just empty messages.
        $this->validateUserMessage($message);

        $botMessages = [];

        if( ! $this->isStarted() ) {
            $botMessages[] = $this->getWelcomeMessage();
        }

        $this->conversationData->addUserConversationMessage($message);

        foreach( $this->attributes as $attributeName => $attribute)
        {
            // Guard clause; if we already have the attribute value, just continue the loop
            if( $attribute->isRetrieved() ) {
                continue;
            }

            $attributeQuestion = $attribute->getAttributeQuestion();

            if( ! $attribute->isAsked() ) {
                $botMessages[] = $attributeQuestion->getQuestion();
                $attribute->setAsked(true);
                $this->conversationData->setAttributeAsAsked($attributeName);
            } else {
                if( ! $attributeQuestion->validate($message) ) {
                    $botMessages[] = $attributeQuestion->getErrorMessage();
                } else {
                    $attribute->setValue($message);
                    $attribute->setRetrieved(true);
                    $this->conversationData->setAttributeValue($attributeName, $message);
                    continue;
                }
            }
            break;
        }

        if( $this->isEnded() ) {
            $botMessages[] = $this->getEndMessage();
        }

        $this->addBotMessages($botMessages);

        return $botMessages;
    }

    /**
     * Just an example of simple message validation before handle any response
     *
     * @param $message
     * @throws \Exception
     */
    public function validateUserMessage($message) {
        if(empty($message)) {
            throw new \Exception('Message cannot be empty');
        }
    }

    /**
     * Conversation has already started?
     * @return bool
     */
    public function isStarted()
    {
        return count($this->conversationData->getConversation()) > 0;
    }

    /**
     * Conversation has already ended?
     * @return bool
     */
    public function isEnded()
    {
        return $this->remainingAttributes() === 0;
    }

    /**
     * Return the number of unanswered questions
     * @return int
     */
    public function remainingAttributes()
    {
        $count = 0;
        foreach($this->attributes as $attribute)
        {
            if(! $attribute->isRetrieved()) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * Add bot messages to conversation data
     * @param $botMessages
     */
    private function addBotMessages(array $botMessages)
    {
        foreach($botMessages as $botMessage)
        {
            $this->conversationData->addBotConversationMessage($botMessage);
        }
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param Attribute[] $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return \Conversation\ConversationDataInterface
     */
    public function getConversationData()
    {
        return $this->conversationData;
    }

    /**
     * @param \Conversation\ConversationDataInterface $conversationData
     */
    public function setConversationData($conversationData)
    {
        $this->conversationData = $conversationData;
    }

    /**
     * @return string
     */
    private function getWelcomeMessage()
    {
        return $this->welcomeMessage;
    }

    /**
     * @param string $welcomeMessage
     */
    public function setWelcomeMessage($welcomeMessage)
    {
        $this->welcomeMessage = $welcomeMessage;
    }

    /**
     * Return ending message just displaying any attribute referenced in the end message string with notation {variableName}
     * @return string
     */
    private function getEndMessage()
    {
        $attributeValues = $this->getAttributeValues();
        extract($attributeValues);
        return preg_replace('/\{([a-z]+)\}/e', "$$1", $this->endMessage);
    }

    /**
     * End message to be displayed. Accepts references to attributes using notation {variableName}
     * @param string $endMessage
     */
    public function setEndMessage($endMessage)
    {
        $this->endMessage = $endMessage;
    }

    /**
     * Return an associative array with key = attribute name, value = value of the attribute.
     * Only return attributes with a value retrieved from the user.
     *
     * @return array
     */
    public function getAttributeValues()
    {
        $attributeValues = [];
        foreach($this->attributes as $attribute)
        {
            if($attribute->isRetrieved()) {
                $attributeValues[$attribute->getName()] = $attribute->getValue();
            }
        }
        return $attributeValues;
    }


}