<?php

namespace Conversation;

/**
 * Represents the basic methods of a question to be used in our conversation
 *
 * Interface AttributeQuestionInterface
 * @package Conversation
 */
interface AttributeQuestionInterface
{
    /**
     * Check if provided value is valid for the attribute
     *
     * @param $value
     * @return boolean
     */
    public function validate($value);

    /**
     * Question to be displayed on attribute request
     *
     * @return string
     */
    public function getQuestion();

    /**
     * Error message to be displayed on attribute validation fail
     *
     * @return string
     */
    public function getErrorMessage();
}