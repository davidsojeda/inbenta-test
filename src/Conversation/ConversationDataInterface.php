<?php

namespace Conversation;

interface ConversationDataInterface
{

    /**
     * Return an associative array with the current attribute data (key = attribute name)
     * Format of each array element, can contain the following fields: [retrieved: bool, asked: bool, value: value]
     *
     * @return array
     */
    public function getAttributes();

    /**
     * Set the value of an attribute
     *
     * @param string $attribute
     * @param $value
     */
    public function setAttributeValue($attribute, $value);

    /**
     * Mark an attribute as asked
     * @param $attribute Attribute name
     */
    public function setAttributeAsAsked($attribute);

    /**
     * Return an array with the current conversation messages.
     * Message format: ['user' => 'user|bot', 'message' => 'message']
     *
     * @return array
     */
    public function getConversation();

    /**
     * Add a user message to the conversation
     *
     * @param string $message
     */
    public function addUserConversationMessage($message);

    /**
     * Add a bot message to the conversation
     *
     * @param string $message
     */
    public function addBotConversationMessage($message);

    /**
     * Reset conversation data to inicial clean state
     */
    public function reset();
}