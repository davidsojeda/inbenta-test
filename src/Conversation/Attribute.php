<?php

namespace Conversation;

class Attribute
{
    private $name;
    private $value = null;
    private $asked = false;
    private $retrieved = false;
    private $attributeQuestion;

    public function __construct($name, array $status = [])
    {
        $this->setName($name);
        foreach($status as $key => $value)
        {
            switch($key)
            {
                case 'value':
                    $this->setValue($value);
                    break;
                case 'asked':
                    $this->setAsked($value);
                    break;
                case 'retrieved':
                    $this->setRetrieved($value);
                    break;
            }
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return boolean
     */
    public function isAsked()
    {
        return $this->asked;
    }

    /**
     * @param boolean $asked
     */
    public function setAsked($asked)
    {
        $this->asked = $asked;
    }

    /**
     * @return boolean
     */
    public function isRetrieved()
    {
        return $this->retrieved;
    }

    /**
     * @param boolean $retrieved
     */
    public function setRetrieved($retrieved)
    {
        $this->retrieved = $retrieved;
    }

    /**
     * @return AttributeQuestionInterface
     */
    public function getAttributeQuestion()
    {
        return $this->attributeQuestion;
    }

    /**
     * @param AttributeQuestionInterface $attributeQuestion
     */
    public function setAttributeQuestion($attributeQuestion)
    {
        $this->attributeQuestion = $attributeQuestion;
    }


}