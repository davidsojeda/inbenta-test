<?php

namespace Conversation\AttributeQuestion;

use Conversation\AttributeQuestionInterface;

class AgeAttributeQuestion implements AttributeQuestionInterface
{
    public function validate($value)
    {
        return ($value > 0 && $value < 140);
    }

    public function getQuestion()
    {
        return 'How old are you?';
    }

    public function getErrorMessage()
    {
        return 'Sorry, I could not understand your age';
    }

}