<?php

namespace Conversation\AttributeQuestion;

use Conversation\AttributeQuestionInterface;

class EmailAttributeQuestion implements AttributeQuestionInterface {

    public function validate($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function getQuestion()
    {
        return 'What is your email?';
    }

    public function getErrorMessage()
    {
        return 'Sorry, I could not understand your email address';
    }

}