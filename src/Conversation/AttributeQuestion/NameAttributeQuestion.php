<?php

namespace Conversation\AttributeQuestion;

use Conversation\AttributeQuestionInterface;

class NameAttributeQuestion implements AttributeQuestionInterface
{
    public function validate($value)
    {
        return true;
    }

    public function getQuestion()
    {
        return 'What is your name?';
    }

    public function getErrorMessage()
    {
        return '';
    }

}