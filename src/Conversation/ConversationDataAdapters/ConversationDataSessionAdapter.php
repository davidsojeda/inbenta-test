<?php

namespace Conversation\ConversationDataAdapters;

use Conversation\ConversationDataInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ConversationDataSessionAdapter implements ConversationDataInterface
{
    private $session;
    private $sessionAttributesKey = 'cd_attributes';
    private $sessionConversationKey = 'cd_conversation';

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function getAttributes()
    {
        return $this->session->get($this->sessionAttributesKey, []);
    }

    public function setAttributeValue($attribute, $value)
    {
        $attributes = $this->getAttributes();
        if(! key_exists($attribute, $attributes)) {
            $attributes[$attribute] = [];
        }
        $attributes[$attribute]['value'] = $value;
        $attributes[$attribute]['retrieved'] = true;
        $this->session->set($this->sessionAttributesKey, $attributes);
    }

    public function setAttributeAsAsked($attribute)
    {
        $attributes = $this->getAttributes();
        if(! key_exists($attribute, $attributes)) {
            $attributes[$attribute] = [];
        }
        $attributes[$attribute]['asked'] = true;
        $attributes[$attribute]['retrieved'] = false;
        $this->session->set($this->sessionAttributesKey, $attributes);
    }

    public function getConversation()
    {
        return $this->session->get($this->sessionConversationKey, []);
    }

    public function addUserConversationMessage($message)
    {
        $this->addConversationMessage('user', $message);
    }

    public function addBotConversationMessage($message)
    {
        $this->addConversationMessage('bot', $message);
    }

    public function reset()
    {
        $this->session->set($this->sessionAttributesKey, []);
        $this->session->set($this->sessionConversationKey, []);
    }

    private function addConversationMessage($user, $message)
    {
        $conversation = $this->getConversation();
        $conversation[] = ['user' => $user, 'message' => $message];
        $this->session->set($this->sessionConversationKey, $conversation);
    }


}