<?php

namespace Conversation\ConversationDataAdapters;

use Conversation\ConversationDataInterface;

/**
 * Class ConversationDataArrayAdapter
 * @package Conversation\ConversationDataAdapters
 *
 * Basic implementation just for testing purposes
 */
class ConversationDataArrayAdapter implements ConversationDataInterface
{
    private $conversation = [];
    private $attributes = [];

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributeValue($attribute, $value)
    {
        if(! key_exists($attribute, $this->attributes)) {
            $this->attributes[$attribute] = [];
        }
        $this->attributes[$attribute]['value'] = $value;
        $this->attributes[$attribute]['retrieved'] = true;
    }

    public function getConversation()
    {
        return $this->conversation;
    }

    public function addUserConversationMessage($message)
    {
        $this->addConversationMessage('user', $message);
    }

    public function addBotConversationMessage($message)
    {
        $this->addConversationMessage('bot', $message);
    }

    private function addConversationMessage($user, $message)
    {
        $this->conversation[] = ['user' => $user, 'message' => $message];
    }

    public function setAttributeAsAsked($attribute)
    {
        if(! key_exists($attribute, $this->attributes)) {
            $this->attributes[$attribute] = [];
        }
        $this->attributes[$attribute]['asked'] = true;
        $this->attributes[$attribute]['retrieved'] = false;
    }

    public function reset()
    {
        $this->conversation = [];
        $this->attributes = [];
    }
}